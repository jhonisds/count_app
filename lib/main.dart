import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      title: "Contador de Aluno",
      home: Home(),
      debugShowCheckedModeBanner: true));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _student = 0;
  String _message = "Temos vagas";
  var _textColor = Colors.green;

  void _changeStudent(int delta) {
    setState(() {
      _student += delta;
      _textColor =  Colors.green;

      if (_student < 0) {
        _message = "Limite abaixo de 0";
        _textColor = Colors.red;
      } else if (_student <= 10) {
        _message = "Vagas Abertas";
      } else {
        _message = "Vagas Esgotadas";
        _textColor = Colors.yellow;
      }
    });
  }

  void _refresh(int delta) {
    setState(() {
      _student = delta;
      _message = "Vagas Abertas";
      _textColor =  Colors.green;
      debugPrint("btn refresh: " + _student.toString() + " message: " + _message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Mini Curso Flutter"),
            backgroundColor: Colors.blueAccent,
            actions: <Widget>[
                IconButton(icon: Icon(Icons.refresh), onPressed: () => _refresh(0),)
            ],),
        body: Stack(
          children: <Widget>[
            Image.asset(
              "images/wallpaper.jpg",
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.cover,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Alunos: $_student",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 32.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FlatButton(
                          onPressed: () {
                            debugPrint("action btn => +1");
                            _changeStudent(1);
                          },
                          child: Text("+1",
                              style: TextStyle(
                                  fontSize: 40.0, color: Colors.white))),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FlatButton(
                          onPressed: () {
                            debugPrint("action btn => -1");
                            _changeStudent(-1);
                          },
                          child: Text("-1",
                              style: TextStyle(
                                  fontSize: 40.0, color: Colors.white))),
                    ),
                  ],
                ),
                Text(
                  _message,
                  style: TextStyle(
                      color: _textColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 24.0),
                )
              ],
            )
          ],
        ));
  }
}
